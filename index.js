const puppeteer = require('puppeteer')
const ora = require('ora')
const Table = require('cli-table')

const spinner = ora('Contacting brekele modem..').start()
const creds = require('./creds')
let table = new Table()

const SMSButtonSelector = '#sms'
const loginModalDialogSelector = 'div.login_dialog'
const usernameInputSelector = 'input#username'
const passwordInputSelector = 'input#password'
const loginButtonSelector = 'input#pop_login'

const newSMSButtonSelector = 'input#message'
const newSMSDialogSelector = 'div#sms_dialog'
const SMSRecipientInputSelector = 'input#recipients_number'
const SMSContentInputSelector = 'textarea#message_content'
const SMSSendButtonSelector = 'input#pop_send'

const SMSSuccessInfoSelector = 'table#sms_success_info'
const SMSSuccessInfoButtonSelector = 'input#pop_OK'

function parseResponseText(text) {
  return text
    .split(',')
    .map(segment => segment.replace('363', ''))
    .map(segment => segment.replace('Sisa kuota', ''))
    .map(segment => {
      if (segment.includes('Beli')) {
        return segment.split('.')[0]
      }
      return segment
    })
}

async function run() {
  const browser = await puppeteer.launch({ headless: false })
  const page = await browser.newPage()

  try {
    await page.goto('http://192.168.8.1/html/home.html')
    await page.waitForSelector(SMSButtonSelector)
    await page.click(SMSButtonSelector)
    await page.waitForSelector(loginModalDialogSelector)
    await page.focus(usernameInputSelector)
    await page.keyboard.type(creds.username)
    await page.focus(passwordInputSelector)
    await page.keyboard.type(creds.password)
    await page.click(loginButtonSelector)

    await page.waitForSelector(newSMSButtonSelector)
    await page.click(newSMSButtonSelector)
    await page.waitForSelector(newSMSDialogSelector)
    await page.focus(SMSRecipientInputSelector)
    await page.keyboard.type('363')
    await page.focus(SMSContentInputSelector)
    await page.keyboard.type('usage')
    await page.click(SMSSendButtonSelector)

    await page.waitForSelector(SMSSuccessInfoSelector)
    await page.click(SMSSuccessInfoButtonSelector)

    setTimeout(async () => {
      const responseText = await page.evaluate(() => {
        const message = document.querySelectorAll('tr.sms_list_tr')[0]
          .textContent
        return message
      })
      spinner.stop()
      table.push(parseResponseText(responseText))
      console.log(table.toString())
    }, 1000)
  } catch (e) {
    spinner.stop()
    console.log('Something wrong')
  }

  browser.close()
}

run()
